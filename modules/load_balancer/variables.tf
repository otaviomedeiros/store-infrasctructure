variable "vpc_id" {
  type        = string
  description = "Id of the VPC"
}

variable "public_subnet_ids" {
  type = list
}

variable "services" {
  type        = set(string)
  description = "List with all the ECS services names"
}
