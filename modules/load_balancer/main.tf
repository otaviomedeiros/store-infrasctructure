resource "aws_security_group" "public_security_group" {
  name   = "Security Group for public access"
  vpc_id = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "load_balancer" {
  name               = "store-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.public_security_group.id]
  subnets            = var.public_subnet_ids
}

resource "aws_lb_target_group" "services_target_group" {
  for_each    = var.services
  name        = "${each.value}-target-group"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = var.vpc_id

  health_check {
    path = "/${each.value}/healthcheck"
  }

  depends_on = [aws_lb.load_balancer]
}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.load_balancer.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.services_target_group[element(tolist(var.services), 0)].arn
  }
}

resource "aws_lb_listener_rule" "service_listener_rule" {
  for_each     = var.services
  listener_arn = aws_lb_listener.alb_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.services_target_group[each.value].arn
  }

  condition {
    path_pattern {
      values = ["/${each.value}*"]
    }
  }
}


