output "alb_security_group_id" {
  value = aws_security_group.public_security_group.id
}

output "target_groups" {
  value = aws_lb_target_group.services_target_group
}

output "load_balancer" {
  value = aws_lb.load_balancer
}


