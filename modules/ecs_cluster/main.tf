resource "aws_ecr_repository" "service_repository" {
  for_each             = var.services
  name                 = each.value
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }
}

resource "aws_ecs_cluster" "store_cluster" {
  name               = var.ecs_cluster_name
  capacity_providers = ["FARGATE"]
}

data "aws_iam_policy_document" "task_policy_document" {
  statement {
    actions   = ["dynamodb:*"]
    resources = var.dynamodb_tables
    effect    = "Allow"
  }
}

resource "aws_iam_policy" "task_policy" {
  name   = "task_policy"
  path   = "/"
  policy = data.aws_iam_policy_document.task_policy_document.json
}

data "aws_iam_policy_document" "task_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "task_role" {
  name               = "service_task_role"
  assume_role_policy = data.aws_iam_policy_document.task_assume_role_policy.json
}

resource "aws_iam_policy_attachment" "task_policy_attachment" {
  name       = "task_policy_attachment"
  roles      = [aws_iam_role.task_role.name]
  policy_arn = aws_iam_policy.task_policy.arn
}

resource "aws_ecs_task_definition" "services_task_definition" {
  for_each = var.services
  family   = "${each.value}-task-definition"
  container_definitions = templatefile("${path.module}/task-definitions/template.json.tpl", {
    service = each.value
    region  = var.region
    port    = var.task_definition_port
  })
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.task_definition_cpu
  memory                   = var.task_definition_memory
  task_role_arn            = aws_iam_role.task_role.arn
  execution_role_arn       = var.task_execution_role_arn
}

resource "aws_ecs_service" "service" {
  for_each        = var.services
  name            = "${each.value}-service"
  cluster         = aws_ecs_cluster.store_cluster.id
  task_definition = aws_ecs_task_definition.services_task_definition[each.value].arn
  desired_count   = 2
  launch_type     = "FARGATE"

  network_configuration {
    subnets          = var.public_subnet_ids
    security_groups  = [aws_security_group.services_security_group.id]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = var.target_groups[each.value].arn
    container_name   = each.value
    container_port   = var.task_definition_port
  }
}

resource "aws_cloudwatch_log_group" "services_log_group" {
  for_each          = var.services
  name              = "/ecs/${each.value}"
  retention_in_days = 30
}

resource "aws_security_group" "services_security_group" {
  name   = "Services Security Group"
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "services_ingress_sg" {
  type                     = "ingress"
  protocol                 = "-1"
  from_port                = var.task_definition_port
  to_port                  = var.task_definition_port
  source_security_group_id = var.load_balancer_security_group_id
  security_group_id        = aws_security_group.services_security_group.id
}

resource "aws_security_group_rule" "services_egress_sg" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.services_security_group.id
}
