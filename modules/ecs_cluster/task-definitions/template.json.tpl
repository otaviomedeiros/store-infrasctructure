[
  {
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "/ecs/${service}",
        "awslogs-region": "${region}",
        "awslogs-stream-prefix": "ecs"
      }
    },
    "portMappings": [
      {
        "hostPort": ${port},
        "protocol": "tcp",
        "containerPort": ${port}
      }
    ],
    "cpu": 0,
    "memoryReservation": 300,
    "image": "065516211023.dkr.ecr.sa-east-1.amazonaws.com/${service}:latest",
    "name": "${service}",
    "environment": [
      {
        "name": "PORT",
        "value": "${port}"
      },
      {
        "name": "REGION",
        "value": "${region}"
      },
      {
        "name": "DB_ENDPOINT",
        "value": "https://dynamodb.${region}.amazonaws.com"
      }
    ]
  }
]
