variable "region" {
  type = string
}

variable "vpc_id" {
  type        = string
  description = "Id of the VPC"
}

variable "public_subnet_ids" {
  type = list
}

variable "load_balancer_security_group_id" {
  type = string
}

variable "target_groups" {
  type = map(object({
    arn = string
  }))
}

variable "services" {
  type        = set(string)
  description = "List with all the ECS services names"
}

variable "ecs_cluster_name" {
  type    = string
  default = "store-cluster"
}

variable "task_definition_cpu" {
  type    = string
  default = 256
}

variable "task_definition_memory" {
  type    = string
  default = 512
}

variable "task_definition_port" {
  type    = string
  default = 3000
}

variable "task_execution_role_arn" {
  type    = string
  default = "arn:aws:iam::065516211023:role/ecsTaskExecutionRole"
}

variable "dynamodb_tables" {
  type = list
}
