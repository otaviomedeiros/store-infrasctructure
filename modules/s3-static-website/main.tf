data "aws_iam_policy_document" "store_products_api_doc_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["arn:aws:s3:::${var.bucket_name}/*"]
    effect    = "Allow"

    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
}

resource "aws_s3_bucket" "products_api_doc_bucket" {
  bucket        = var.bucket_name
  acl           = "public-read"
  policy        = data.aws_iam_policy_document.store_products_api_doc_policy.json
  force_destroy = true

  website {
    index_document = "index.html"
  }
}
