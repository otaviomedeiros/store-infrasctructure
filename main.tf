provider "aws" {
  profile = var.profile
  region  = var.region
}

locals {
  services = toset(var.ecs_service_names)
}

module "load_balancer" {
  source = "./modules/load_balancer"

  services          = local.services
  vpc_id            = var.vpc_id
  public_subnet_ids = var.public_subnet_ids
}

module "databases" {
  source = "./modules/database"
}

module "ecs_modules" {
  source = "./modules/ecs_cluster"

  services                        = local.services
  region                          = var.region
  vpc_id                          = var.vpc_id
  public_subnet_ids               = var.public_subnet_ids
  load_balancer_security_group_id = module.load_balancer.alb_security_group_id
  target_groups                   = module.load_balancer.target_groups
  dynamodb_tables                 = [module.databases.customers_table]
}

module "products_api_documentation" {
  source = "./modules/s3-static-website"

  bucket_name = "store-products-api-doc"
}


