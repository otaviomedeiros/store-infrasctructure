profile           = "otavio"
region            = "sa-east-1"
vpc_id            = "vpc-2dcafd48"
public_subnet_ids = ["subnet-c47389a0", "subnet-f1014186"]
ecs_service_names = ["customers", "products"]
