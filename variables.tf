variable "profile" {
  type        = string
  description = "Profile to access aws account"
}

variable "region" {
  type = string
}

variable "vpc_id" {
  type        = string
  description = "Id of the VPC"
}

variable "ecs_service_names" {
  type        = list(string)
  description = "List with all the ECS services names"
}

variable "public_subnet_ids" {
  type = list
}






